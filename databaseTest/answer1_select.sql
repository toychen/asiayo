--目前 AsiaYo 營運人員想知道 2021 年 2 月, 訂單數量前 10 名的旅宿,請用一條 SQL 查詢出結果。

SELECT 
    `property`.`name`,
    COUNT(`orders`.`id`) AS order_count
FROM `orders`
INNER JOIN `room` ON `orders`.`room_id` = `room`.`id`
INNER JOIN `property` ON `room`.`property_id` = `property`.`id`
WHERE `orders`.`created_at` >= '2021-02-01 00:00:00'
AND `orders`.`created_at` < '2021-03-01 00:00:00'
GROUP BY `property`.`id`
ORDER BY order_count DESC
LIMIT 0,10;