根據題目一的查詢,發現查詢速度過慢(超過 10 秒),
您覺得問題可能會是什麼?
您會嘗試如何調整?請試著闡述您的方法。

------------------------------------------------------
先用DESC查看效能
例如:
DESC
SELECT 
    `property`.`name`,
    COUNT(`orders`.`id`) AS order_count
FROM `orders`
INNER JOIN `room` ON `orders`.`room_id` = `room`.`id`
INNER JOIN `property` ON `room`.`property_id` = `property`.`id`
WHERE `orders`.`created_at` >= '2021-02-01 00:00:00'
AND `orders`.`created_at` < '2021-03-01 00:00:00'
GROUP BY `property`.`id`
ORDER BY order_count DESC
LIMIT 0,10;

1. 檢查是否都有使用keys，若有索引但沒使用，可強制指定indexing試試
2. 若要增加indexing，要檢查資料表大小，太大的話建議增加 ALGORITHM=INPLACE,LOCK=NONE; 避免鎖表
3. 檢查查詢基數有多少，真的太大的話，可以先取得時間區間內room_id的訂單數量後，才join property