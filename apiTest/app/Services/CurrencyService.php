<?php

namespace App\Services;

class CurrencyService
{
    const CURRENCIES = [
        'TWD' =>  [
            'TWD' => 1,
            'JPY' => 3.669,
            'USD' => 0.03281
        ],
        'JPY' =>  [
            'TWD' => 0.26956,
            'JPY' => 1,
            'USD' => 0.00885
        ],
        'USD' =>  [
            'TWD' => 30.444,
            'JPY' => 111.801,
            'USD' => 1
        ]
    ];

    /**
     * 轉換匯率.
     *
     * @param float $originCurrency 來源幣別
     * @param float $targetCurrency 目標幣別
     * @param float $amount 金額數字
     *
     * @return float
     */
    public function exchange(
        $originCurrency,
        $targetCurrency,
        $amount = 0
    ) {
        $targetRate = self::CURRENCIES[$originCurrency][$targetCurrency] ?? 0;
        
        return floatval(round($amount * $targetRate, 2));
    }
}
