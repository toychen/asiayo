<?php
/**
 * @OA\Info(
 *      version="1.0.0",
 *      title="Currency API",
 *      description="Currency API",
 *      @OA\Contact(
 *          email="leo78707@gmail.com"
 *      ),
 *     @OA\License(
 *         name="Apache 2.0",
 *         url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *     )
 * )
 *
 * @OA\server(
 *      url = "http://localhost",
 *      description="Localhost"
 * )
 * 
 * @OA\Response(
 *     response=401,
 *     description="Unauthorized",
 *     @OA\JsonContent(type="object",
 *          @OA\Property( property="status", type="string", example="success"),
 *          @OA\Property( property="code", type="string", example="401"),
 *          @OA\Property( property="message", type="string", example="Unauthorized."),
 *     )
 * )
 * 
 * @OA\Response(response=200, description="Successful Operation",
 *      @OA\JsonContent(type="object",
 *          @OA\Property( property="status", type="string", example="success"),
 *          @OA\Property( property="code", type="string", example="200"),
 *          @OA\Property( property="message", type="string", example=""),
 *      )
 * )
 * 
 */