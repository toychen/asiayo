<?php

/**
 * @OA\Get(
 *     tags={"currency"},
 *     path="/",
 *     summary="匯率計算",
 *     description="匯率計算",
 *     operationId="getCurrency",
 *     @OA\Parameter(name="originCurrency", in="query", description="來源幣別", required=true,
 *         @OA\Schema(type="string", enum={"TWD", "JPY", "USD"})
 *     ),
 *     @OA\Parameter(name="targetCurrency", in="query", description="目標幣別", required=true,
 *         @OA\Schema(type="string", enum={"TWD", "JPY", "USD"})
 *     ),
 *     @OA\Parameter(name="amount", in="query", description="金額數字", required=true,
 *         @OA\Schema(type="number", format="float")
 *     ),
 *     @OA\Response(
 *          response=200,
 *          description="Successful operation",
 *          @OA\JsonContent(type="object",
 *              @OA\Property( property="code", type="string", example="200"),
 *              @OA\Property( property="status", type="string", example="success"),
 *              @OA\Property( property="data", type="object",
 *                  @OA\Property( property="currency", type="string", description="幣別", example="USD"),
 *                  @OA\Property( property="amount", type="string", description="格式化後金額", example="1,262.48"),
 *              ),
 *          ),
 *     ),
 *     @OA\Response(
 *         response=401,
 *         ref="#/components/responses/401"
 *     )
 * )
 */