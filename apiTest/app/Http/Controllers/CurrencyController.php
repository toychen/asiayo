<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Services\CurrencyService;
use Illuminate\Http\Request;
//use App\Http\Resources\CurrencyResource;
use Illuminate\Validation\ValidationException;


class CurrencyController extends Controller
{
    protected $currencyService;

    public function __construct(CurrencyService $currencyService)
    {
        $this->currencyService = $currencyService;
    }

    public function index(Request $request)
    {
        $currencies = implode(',', array_keys($this->currencyService::CURRENCIES));
        try {
            $this->validate($request, [ 
                'originCurrency' => 'required|in:' . $currencies, 
                'targetCurrency' => 'required|in:' . $currencies, 
                'amount' => 'required|numeric|min:0' 
            ]);
        } catch (ValidationException $exception) {
            return response()->json([
                'code' => 401,
                'status' => 'error',
                'message'=> $exception->validator->getMessageBag()->getMessages()
            ], 401);
        }

        $originCurrency = $request->input('originCurrency');
        $targetCurrency = $request->input('targetCurrency');
        $amount = $request->input('amount');
        $anwser = $this->currencyService->exchange(
            $originCurrency,
            $targetCurrency,
            $amount,
        );
        return response()->json([
            'code' => 200,
            'status' => 'success',
            'data'=> [
                'currency' => $targetCurrency,
                'amount' => number_format($anwser, 2),
            ]
        ]);
    }
}