<?php
use App\Services\CurrencyService as CurrencyService;
class CurrencyTest extends TestCase
{
    public function testService()
    {
        $service = new CurrencyService;
        $amount = $service->exchange('JPY', 'JPY', 55.55555);
        $this->assertEquals(55.56, $amount);
    }

    public function testAPI()
    {
        $this->get("/?originCurrency=TWD&targetCurrency=JPY&amount=90000", []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure(
            [
                'data' => [
                    'currency',
                    'amount'
                ]
            ]    
        );
    }
}